package fr.afcepf.al33.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Prestataire 
{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id", nullable=false)
	private int id;
	private String nom;
	private String prenom;
	private String description;
	private String email;
	private String telephone;
	private Double tauxHoraire;
	private String photo;
	
	@JsonIgnore
	@ManyToOne
	private TypePrestation prestation;
	
	// Getter Setter et Constructeur --------------------------------------------------------------------------
	
	public Prestataire() {
		super();
	}
	
	public Prestataire(int id, String nom, String prenom, String description, String email, String telephone, Double tauxHoraire, String photo, TypePrestation prestation) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.description = description;
		this.email = email;
		this.telephone = telephone;
		this.tauxHoraire = tauxHoraire;
		this.photo = photo;
		this.prestation = prestation;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public Double getTauxHoraire() {
		return tauxHoraire;
	}
	public void setTauxHoraire(Double tauxHoraire) {
		this.tauxHoraire = tauxHoraire;
	}

	public TypePrestation getPrestation() {
		return prestation;
	}

	public void setPrestation(TypePrestation prestation) {
		this.prestation = prestation;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
