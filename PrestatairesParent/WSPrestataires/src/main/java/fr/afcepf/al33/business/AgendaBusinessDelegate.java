package fr.afcepf.al33.business;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import Dto.Periode;
import fr.afcepf.al33.dto.ListPrestataireReferences;
import fr.afcepf.al33.dto.LockInfos;
import fr.afcepf.al33.dto.PrestataireReference;


@Service
public class AgendaBusinessDelegate implements AgendaIBusinessDelegate {

	public RestTemplate restTemplate()
	{
		return new RestTemplate();
	}
	
	@Override
	public ListPrestataireReferences tousPrestatairesDispoParPeriode(Periode periode) 
	{
		DateFormat shortDateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT,DateFormat.SHORT);

		final String uri = "http://localhost:9890/WSAgenda/dates/dispo";    //post/envoi:  
		// DTO période dateDebut et dateFin : String
		fr.afcepf.al33.dto.Periode myPeriode = new fr.afcepf.al33.dto.Periode();
		try {
			myPeriode.setDateDebut(shortDateFormat.parse(periode.getDateDebut()));
			myPeriode.setDateFin(shortDateFormat.parse(periode.getDateFin()));
			System.out.println(" Conversion Date : " + myPeriode.getDateDebut() + " - " + myPeriode.getDateFin());
		} catch (ParseException e) {
			e.printStackTrace();
		}		
		ListPrestataireReferences prestRefs = restTemplate().postForObject(uri, myPeriode, ListPrestataireReferences.class); 
		return prestRefs;
	}
	
	@Override
	public boolean reserverPrestatairePeriode(String kikatou, int idPrestataire, Periode periode)
	{
		DateFormat shortDateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT,DateFormat.SHORT);

		final String uri = "http://localhost:9890/WSAgenda/dates/lock";    //post/envoi:    
		LockInfos lockInfos = new LockInfos();
		lockInfos.setIdPrestataire(idPrestataire);
		lockInfos.setLockedBy(kikatou);
		System.out.println("periode.dateDebut:" + periode.getDateDebut());
		try {
			lockInfos.setStart(shortDateFormat.parse(periode.getDateDebut()));
			System.out.println("lockinfos:" + lockInfos.getStart());
			lockInfos.setEnd(shortDateFormat.parse(periode.getDateFin()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		boolean result = restTemplate().postForObject(uri, lockInfos, boolean.class); 
		return result;
	}

	@Override
	public boolean annulerPrestatairePeriode(String kikatou, int idPrestataire, Periode periode)
	{
		DateFormat shortDateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT,DateFormat.SHORT);

		final String uri = "http://localhost:9890/WSAgenda/dates/unlock";    //post/envoi:    
		LockInfos lockInfos = new LockInfos();
		lockInfos.setIdPrestataire(idPrestataire);
		lockInfos.setLockedBy(kikatou);
		System.out.println("periode.dateDebut:" + periode.getDateDebut());
		try {
			lockInfos.setStart(shortDateFormat.parse(periode.getDateDebut()));
			System.out.println("lockinfos:" + lockInfos.getStart());
			lockInfos.setEnd(shortDateFormat.parse(periode.getDateFin()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		boolean result = restTemplate().postForObject(uri, lockInfos, boolean.class); 
		return result;
	}
	
}
