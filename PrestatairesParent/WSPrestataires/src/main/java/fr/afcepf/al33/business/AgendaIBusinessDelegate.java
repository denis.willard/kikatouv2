package fr.afcepf.al33.business;

import java.util.List;

import Dto.Periode;
import fr.afcepf.al33.dto.ListPrestataireReferences;
import fr.afcepf.al33.dto.PrestataireReference;

public interface AgendaIBusinessDelegate 
{
	ListPrestataireReferences tousPrestatairesDispoParPeriode(Periode periode);
	boolean reserverPrestatairePeriode(String kikatou, int idPrestataire, Periode periode);
	boolean annulerPrestatairePeriode(String societe, int intValue, Periode periode);
}
