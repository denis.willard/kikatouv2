package fr.afcepf.al33.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.afcepf.al33.business.TypePrestationBusiness;
import fr.afcepf.al33.entity.TypePrestation;

@CrossOrigin("*")
@RestController
@RequestMapping(value="/rest/public/types_prestations", headers="Accept=application/json")
public class TypePrestationRestCtrl 
{
	@Autowired
	private TypePrestationBusiness proxyTypePrestation;
	
	@GetMapping()
	public List<TypePrestation> getTypesPrestations ()
	{
		return proxyTypePrestation.getTypesPrestations();
	}
	
}
