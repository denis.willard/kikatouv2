package Dto;

import java.io.Serializable;

public class Prestataire implements Serializable
{

	private static final long serialVersionUID = 1L;

	private int id;
	private String nom;
	private String prenom;
	private String description;
	private String email;
	private String telephone;
	private Double tauxHoraire;
	private String photo;
	
	// Getter Setter et Constructeur --------------------------------------------------------------------------

	public Prestataire() {
		super();
	}
	
	public Prestataire(int id, String nom, String prenom, String description, String email, String telephone, Double tauxHoraire,
			String photo) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.description = description;
		this.email = email;
		this.telephone = telephone;
		this.tauxHoraire = tauxHoraire;
		this.photo = photo;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public Double getTauxHoraire() {
		return tauxHoraire;
	}
	public void setTauxHoraire(Double tauxHoraire) {
		this.tauxHoraire = tauxHoraire;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
