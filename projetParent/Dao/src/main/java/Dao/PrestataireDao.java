package Dao;

import Entities.Prestation;
import IDao.PrestationIDao;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Remote(PrestationIDao.class)
@Stateless
public class PrestataireDao extends DAO implements PrestationIDao {


    @PersistenceContext(unitName="DidiDS")
    private EntityManager em;


    @Override
    public List<Prestation> findByIdEvent(int id) {
        Query query = em.createQuery("SELECT plop from Prestation plop WHERE plop.event.id=:id",Prestation.class);
        query.setParameter("id", id);
        List<Prestation> prestations = query.getResultList();
        return prestations;
    }

    @Override
    public void deletePrestation(Prestation p) {
        Prestation prestRemove= em.getReference(Prestation.class, p.getId());
        em.remove(prestRemove);

    }
}
