package dtoClients;

import java.io.Serializable;

public class MessageDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	public MessageDTO() {
    }

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
