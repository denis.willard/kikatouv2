package Business;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import Entities.User;
import IBusiness.UserIBusiness;
import IDao.UserIDao;
import dtoClients.*;

@Remote(UserIBusiness.class)
@Stateless
public class UserBusiness implements UserIBusiness {

    @EJB
    private UserIDao proxyUser;

    @Override
    public User createUser(User user) {
        user = proxyUser.create(user);
        user.getClientDTO().setIdUtilisateur(user.getId());
        Client client = ClientBuilder.newClient();
        Response response = client.target("http://localhost:9490/Orchestrateur/client/inscription")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(user.getClientDTO(), MediaType.APPLICATION_JSON_TYPE));
        ClientDTO clientDTO = response.readEntity(ClientDTO.class);
        user.setClientDTO(clientDTO);
        return user;
    }

    @Override
    public boolean existeDejaMail(String mail) {
        Client client = ClientBuilder.newClient();
        boolean existeDeja = client.target("http://localhost:9290/WSUser/rest/ajouter/mail")
                .path(mail)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(Boolean.TYPE);
        return existeDeja;
    }

    @Override
    public boolean existeDejaLogin(String login) {
        Client client = ClientBuilder.newClient();
        boolean existeDeja = client.target("http://localhost:9290/WSUser/rest/ajouter/login")
                .path(login)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(Boolean.TYPE);
        return existeDeja;
    }

    @Override
    public String delete(User user) {
        Client client = ClientBuilder.newClient();
        Response response = client.target("http://localhost:9490/Orchestrateur/client/desinscription")
                .path(user.getId().toString())
                .request(MediaType.APPLICATION_JSON_TYPE)
                .delete();
        MessageDTO messageDTO = response.readEntity(MessageDTO.class);
        System.out.println(messageDTO.getMessage().toUpperCase());
        proxyUser.delete(user);
        return messageDTO.getMessage();
    }

    @Override
    public String update(User user) {
        Client client = ClientBuilder.newClient();
        Response response = client.target("http://localhost:9490/Orchestrateur/client/modification")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .put(Entity.entity(user.getClientDTO(), MediaType.APPLICATION_JSON_TYPE));
        MessageDTO messageDTO = response.readEntity(MessageDTO.class);
        proxyUser.update(user);
        return messageDTO.getMessage();
    }

    @Override
    public User find(int id) {
        Client client = ClientBuilder.newClient();
        ClientDTO clientDTO = client
                .target("http://localhost:9490/Orchestrateur/client")
                .path(Integer.toString(id))
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(ClientDTO.class);
        User user = proxyUser.find(User.class, id);
        user.setClientDTO(clientDTO);
        return user;
    }

    @Override
    public User connection(String log, String mdp) {
        Client client = ClientBuilder.newClient();
        ConnexionDTO connexionDTO = client.target("http://localhost:9490/Orchestrateur/client/connexion")
                .path(log)
                .path(mdp)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(ConnexionDTO.class);
        User user;
        if (connexionDTO.isConnexionOK()) {
            user = proxyUser.find(User.class, connexionDTO.getClientDTO().getIdUtilisateur());
            user.setClientDTO(connexionDTO.getClientDTO());
            return user;
        }
        return null;
    }

    @Override
    public List<User> getAll() {
        Client client = ClientBuilder.newClient();
        ClientListDTO clientListDTO = client.target("http://localhost:9490/Orchestrateur/client")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(ClientListDTO.class);
        List<User> users = new ArrayList<>();
        for (ClientDTO clientDTO : clientListDTO.getClients()) {
            System.out.println("RECHERCHE " + clientDTO.getIdUtilisateur());
            User user = proxyUser.find(User.class, clientDTO.getIdUtilisateur());
            System.out.println("TROUVE " + clientDTO.getIdUtilisateur());
            user.setClientDTO(clientDTO);
            users.add(user);
        }
        return users;
    }

}
