package Business;

import Dto.Prestataire;
import Entities.Commande;
import Entities.Prestation;
import IBusiness.CommandeIBusiness;
import IBusiness.PrestationIBusiness;
import IDao.CommandeIDao;
import IDao.PrestationIDao;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Remote(PrestationIBusiness.class)
@Stateless
public class PrestationBusiness implements PrestationIBusiness {

    private String url ="http://localhost:9190/WSPrestataires/rest/public/";

    @EJB
    private PrestationIDao proxyPrestation;

    @Override
    public Prestation create(Prestation p) {
        proxyPrestation.create(p);
        return p;
    }

    @Override
    public Prestation delete(Prestation p) {
        proxyPrestation.delete(p);
        return p;
    }

    @Override
    public Prestation update(Prestation p) {
        proxyPrestation.update(p);
        return p;
    }

    @Override
    public Prestation find(int id) {

        return proxyPrestation.find(Prestation.class, id);
    }

    @Override
    public List<Prestation> findByIdEvent(int id) {
        List<Prestation> prestations = proxyPrestation.findByIdEvent(id);
        Client client = ClientBuilder.newClient();
        for (Prestation prestation : prestations) {
            Prestataire prestataire = client.target(url)
                    .path("prestataire")
                    .queryParam("idPrestataire", prestation.getIdPrestataire()).request(MediaType.APPLICATION_JSON_TYPE).get(Prestataire.class);
            prestation.setPrestataire(prestataire);
        }
        return prestations;
    }

    @Override
    public void deletePrestation(Prestation p) {
        proxyPrestation.deletePrestation(p);
    }

}
