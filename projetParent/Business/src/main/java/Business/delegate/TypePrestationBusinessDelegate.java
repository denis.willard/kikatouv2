package Business.delegate;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

//import org.glassfish.jersey.jackson.JacksonFeature;

import Dto.TypePrestation;
import IBusiness.delegate.TypePrestationIBusinessDelegate;

@Remote(TypePrestationIBusinessDelegate.class)
@Stateless
public class TypePrestationBusinessDelegate implements TypePrestationIBusinessDelegate 
{
	private Client jaxrs2client;
	private String url ="http://localhost:9190/WSPrestataires/rest/public/"; 
	
	public TypePrestationBusinessDelegate()
	{
		this.jaxrs2client = ClientBuilder.newClient();//.register(JacksonFeature.class);
	}

	@Override
	public List<TypePrestation> tousTypesPrestations() 
	{

		WebTarget convTarget = jaxrs2client.target(url)
											.path("types_prestations");

		Response response = convTarget.request(MediaType.APPLICATION_JSON_TYPE).get();	// appel en GET
		List<TypePrestation> prestations = response.readEntity(new GenericType<List<TypePrestation>>() {});
		
		return prestations;
	}

}
