package Business.delegate;

import IBusiness.delegate.IMailBusinessDelegate;
import IBusiness.delegate.soap.mail.IWSMail;
import IBusiness.delegate.soap.mail.InfoMailDTO;
import IBusiness.delegate.soap.mail.WSMailService;

import java.net.MalformedURLException;
import java.net.URL;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

@Remote(IMailBusinessDelegate.class)
@Stateless
public class MailBusinessDelegate implements IMailBusinessDelegate {

    @Override
    public void envoyerMailConfirmation(InfoMailDTO info) {
    	URL urlWsdl = null;
    	try {
			urlWsdl = new URL("http://localhost:7979/WSMail/WSMail?wsdl");
		} catch (MalformedURLException e) {
			System.out.println("Lien vers WSDL introuvable!");
		}
    	WSMailService s = new WSMailService(urlWsdl);
    	
        IWSMail serviceMail = s.getWSMailPort();
        serviceMail.envoyerMail(info);
    }
}
