package Business.delegate;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

//import org.glassfish.jersey.jackson.JacksonFeature;

import Dto.Periode;
import Dto.Prestataire;
import Dto.TypePrestation;
import IBusiness.ArticleIBusiness;
import IBusiness.delegate.PrestataireIBusinessDelegate;

@Remote(PrestataireIBusinessDelegate.class)
@Stateless
public class PrestataireBusinessDelegate implements PrestataireIBusinessDelegate 
{
	private Client jaxrs2client;
	private String url ="http://localhost:9190/WSPrestataires/rest/public/"; 
	
	public PrestataireBusinessDelegate()
	{
		this.jaxrs2client = ClientBuilder.newClient();//.register(JacksonFeature.class);
	}

	@Override
	public List<Prestataire> tousPrestataires() {

		WebTarget convTarget = jaxrs2client.target(url)
											.path("prestataires");
		
		Response response = convTarget.request(MediaType.APPLICATION_JSON_TYPE).get();	// appel en GET
		List<Prestataire> prestataires = response.readEntity(new GenericType<List<Prestataire>>() {});
		System.out.println(prestataires);
		return prestataires;
	}

	@Override
	public List<Prestataire> tousPrestatairesParTypePrestation(int idPrestation) {

		WebTarget convTarget = jaxrs2client.target(url)
											.path("prestataires")
											.queryParam("idPrestation", idPrestation);
		
		Response response = convTarget.request(MediaType.APPLICATION_JSON_TYPE).get();	// appel en GET
		System.out.println("Réponse!!!");
		List<Prestataire> prestataires = response.readEntity(new GenericType<List<Prestataire>>() {});
		System.out.println(prestataires);				
		return prestataires;
	}

	@Override
	public List<Prestataire> prestatairesParTypePrestationEtPeriode(int idPrestation, Periode periode) {

		WebTarget convTarget = jaxrs2client.target(url)
											.path("prestataires/periode")
											.queryParam("idPrestation", idPrestation);
		
		//String responseString = convTarget.request(MediaType.APPLICATION_JSON).post(Entity.entity(periode,MediaType.APPLICATION_JSON));	// appel en POST
		Response response = convTarget.request(MediaType.APPLICATION_JSON_TYPE).post(Entity.entity(periode,MediaType.APPLICATION_JSON_TYPE));	// appel en POST
		System.out.println(response);
		List<Prestataire> prestataires = response.readEntity(new GenericType<List<Prestataire>>() {});
		System.out.println("prestatairesParTypePrestationEtPeriode: " + prestataires);				
		return prestataires;
	}
	
	@Override
	public Prestataire reserverPrestatairesPourPeriode(int idPrestataire, Periode periode) {

		System.out.println("Id période " + idPrestataire + " Période début " + periode.getDateDebut() + " Période fin " + periode.getDateFin());
		WebTarget convTarget = jaxrs2client.target(url)
											.path("reserverPrestataire/periode")
											.queryParam("idPrestataire", idPrestataire)
											.queryParam("Societe", "KIKATOU");
		
		Response response = convTarget.request(MediaType.APPLICATION_JSON_TYPE).post(Entity.entity(periode,MediaType.APPLICATION_JSON_TYPE));	// appel en POST
		System.out.println(response);
		Prestataire prestataires = response.readEntity(new GenericType<Prestataire>() {});
		System.out.println("reserverPrestatairesPourPeriode:" + prestataires);				
		return prestataires;
	}

	@Override
	public void annulerPrestatairesPourPeriode(int idPrestataire, Periode periode) {

		WebTarget convTarget = jaxrs2client.target(url)
				.path("annulerPrestataire/periode")
				.queryParam("idPrestataire", idPrestataire)
				.queryParam("Societe", "KIKATOU");

		convTarget.request(MediaType.APPLICATION_JSON_TYPE).post(Entity.entity(periode, MediaType.APPLICATION_JSON_TYPE));
	}
}
