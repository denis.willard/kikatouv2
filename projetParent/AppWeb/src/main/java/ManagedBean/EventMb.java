package ManagedBean;

import java.io.Serializable;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import Entities.Event;
import Entities.Ville;
import IBusiness.EventIBusiness;
import IBusiness.UserIBusiness;
import IBusiness.VilleIBusiness;

@ManagedBean(name = "mbEvent")
@SessionScoped
public class EventMb implements Serializable {

    private static final long serialVersionUID = 1L;

    @EJB
    private EventIBusiness proxyEvent;

    @EJB
    private UserIBusiness proxyUser;

    private Date maintenant = new Date();

    @EJB
    private VilleIBusiness proxyVille;

    private Event ajoutEvent = new Event();

    @ManagedProperty(value = "#{mblogin}")
    private LoginMb loginMb;

    @ManagedProperty(value = "#{mbVille}")
    private VilleMb villeMb;

    public long getDureeEvent() {
        long diffInMillies = Math.abs(loginMb.getEvent().getDateDebut().getTime() - loginMb.getEvent().getDateFin().getTime());
        long diffDays = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
        return diffDays+1;
    }

    public String addEvent() {
        if (loginMb.getUser() != null) {
            ajoutEvent.setUser(loginMb.getUser());
            Ville v = proxyVille.create(villeMb.getVille());
            ajoutEvent.setVille(v);
            loginMb.setEvent(proxyEvent.create(ajoutEvent));
            return "/categorie/affichageSousCat.xhtml";
        } else {
            return "/connexion.xhtml";
        }
    }

    public String majEvent() {
        proxyEvent.update(loginMb.getEvent());
        return "/categorie/affichageSousCat.xhtml";
    }

    public EventIBusiness getProxyEvent() {
        return proxyEvent;
    }

    public void setProxyEvent(EventIBusiness proxyEvent) {
        this.proxyEvent = proxyEvent;
    }

    public Event getAjoutEvent() {
        return ajoutEvent;
    }

    public void setAjoutEvent(Event ajoutEvent) {
        this.ajoutEvent = ajoutEvent;
    }

    public UserIBusiness getProxyUser() {
        return proxyUser;
    }

    public void setProxyUser(UserIBusiness proxyUser) {
        this.proxyUser = proxyUser;
    }


    public LoginMb getLoginMb() {
        return loginMb;
    }


    public void setLoginMb(LoginMb loginMb) {
        this.loginMb = loginMb;
    }


    public VilleIBusiness getProxyVille() {
        return proxyVille;
    }


    public void setProxyVille(VilleIBusiness proxyVille) {
        this.proxyVille = proxyVille;
    }


    public VilleMb getVilleMb() {
        return villeMb;
    }

    public void setVilleMb(VilleMb villeMb) {
        this.villeMb = villeMb;
    }

    public Date getMaintenant() {
        return maintenant;
    }
}
