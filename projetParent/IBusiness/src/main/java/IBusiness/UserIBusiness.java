package IBusiness;

import java.util.List;

import Entities.User;

public interface UserIBusiness {
	
	User createUser(User user);

    boolean existeDejaMail(String mail);

	boolean existeDejaLogin(String login);

	String delete (User user);
	String update (User user);
	User find (int id);
	User connection (String log, String mdp);
	List<User> getAll();
}
