package IBusiness;

import java.util.List;

import Entities.Categorie;

public interface CategorieIBusiness {
	
	List<Categorie> afficherCategorie();
	Categorie getCategorieById(int id);


}
