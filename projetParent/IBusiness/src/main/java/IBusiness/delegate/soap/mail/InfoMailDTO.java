
package IBusiness.delegate.soap.mail;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;


/**
 * <p>Classe Java pour infoMailDTO complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="infoMailDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="adressePrestataire" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dateDebutReservation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="duree" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomClient" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomPrestataire" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prenomClient" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prenomPrestataire" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "infoMailDTO", propOrder = {
    "adressePrestataire",
    "dateDebutReservation",
    "duree",
    "nomClient",
    "nomPrestataire",
    "prenomClient",
    "prenomPrestataire"
})
public class InfoMailDTO implements Serializable {
	private static final long serialVersionUID = 1L;

    protected String adressePrestataire;
    protected String dateDebutReservation;
    protected String duree;
    protected String nomClient;
    protected String nomPrestataire;
    protected String prenomClient;
    protected String prenomPrestataire;

    /**
     * Obtient la valeur de la propri�t� adressePrestataire.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdressePrestataire() {
        return adressePrestataire;
    }

    /**
     * D�finit la valeur de la propri�t� adressePrestataire.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdressePrestataire(String value) {
        this.adressePrestataire = value;
    }

    /**
     * Obtient la valeur de la propri�t� dateDebutReservation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateDebutReservation() {
        return dateDebutReservation;
    }

    /**
     * D�finit la valeur de la propri�t� dateDebutReservation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateDebutReservation(String value) {
        this.dateDebutReservation = value;
    }

    /**
     * Obtient la valeur de la propri�t� duree.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDuree() {
        return duree;
    }

    /**
     * D�finit la valeur de la propri�t� duree.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDuree(String value) {
        this.duree = value;
    }

    /**
     * Obtient la valeur de la propri�t� nomClient.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomClient() {
        return nomClient;
    }

    /**
     * D�finit la valeur de la propri�t� nomClient.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomClient(String value) {
        this.nomClient = value;
    }

    /**
     * Obtient la valeur de la propri�t� nomPrestataire.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomPrestataire() {
        return nomPrestataire;
    }

    /**
     * D�finit la valeur de la propri�t� nomPrestataire.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomPrestataire(String value) {
        this.nomPrestataire = value;
    }

    /**
     * Obtient la valeur de la propri�t� prenomClient.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrenomClient() {
        return prenomClient;
    }

    /**
     * D�finit la valeur de la propri�t� prenomClient.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrenomClient(String value) {
        this.prenomClient = value;
    }

    /**
     * Obtient la valeur de la propri�t� prenomPrestataire.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrenomPrestataire() {
        return prenomPrestataire;
    }

    /**
     * D�finit la valeur de la propri�t� prenomPrestataire.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrenomPrestataire(String value) {
        this.prenomPrestataire = value;
    }

}
