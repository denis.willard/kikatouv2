
package IBusiness.delegate.soap.mail;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the IBusiness.delegate.soap.mail package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _EnvoyerMailResponse_QNAME = new QName("http://wsmail.afcepf.fr", "envoyerMailResponse");
    private final static QName _EnvoyerMail_QNAME = new QName("http://wsmail.afcepf.fr", "envoyerMail");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: IBusiness.delegate.soap.mail
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EnvoyerMailResponse }
     * 
     */
    public EnvoyerMailResponse createEnvoyerMailResponse() {
        return new EnvoyerMailResponse();
    }

    /**
     * Create an instance of {@link EnvoyerMail }
     * 
     */
    public EnvoyerMail createEnvoyerMail() {
        return new EnvoyerMail();
    }

    /**
     * Create an instance of {@link InfoMailDTO }
     * 
     */
    public InfoMailDTO createInfoMailDTO() {
        return new InfoMailDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnvoyerMailResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsmail.afcepf.fr", name = "envoyerMailResponse")
    public JAXBElement<EnvoyerMailResponse> createEnvoyerMailResponse(EnvoyerMailResponse value) {
        return new JAXBElement<EnvoyerMailResponse>(_EnvoyerMailResponse_QNAME, EnvoyerMailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnvoyerMail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsmail.afcepf.fr", name = "envoyerMail")
    public JAXBElement<EnvoyerMail> createEnvoyerMail(EnvoyerMail value) {
        return new JAXBElement<EnvoyerMail>(_EnvoyerMail_QNAME, EnvoyerMail.class, null, value);
    }

}
