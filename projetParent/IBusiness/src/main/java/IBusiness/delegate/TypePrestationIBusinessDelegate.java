package IBusiness.delegate;

import java.util.List;
import Dto.TypePrestation;

public interface TypePrestationIBusinessDelegate 
{
	List<TypePrestation> tousTypesPrestations();
}
