package fr.afcepf.al33.dto;

public class AutorisationDTO {

	private String message;
	private boolean estAccepte;
	
	public AutorisationDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public AutorisationDTO(String message, boolean estAccepte) {
		super();
		this.message = message;
		this.estAccepte = estAccepte;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public boolean isEstAccepte() {
		return estAccepte;
	}
	public void setEstAccepte(boolean estAccepte) {
		this.estAccepte = estAccepte;
	}
	
}
