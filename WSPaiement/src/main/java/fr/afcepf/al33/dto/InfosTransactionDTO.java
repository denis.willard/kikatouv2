package fr.afcepf.al33.dto;

public class InfosTransactionDTO {


	private InfosBancaireDTO infosBancaireDTO;
	private String ibanVendeur;
	private Double montant;
	public InfosTransactionDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public InfosTransactionDTO(InfosBancaireDTO infosBancaireDTO, String ibanVendeur, Double montant) {
		super();
		this.infosBancaireDTO = infosBancaireDTO;
		this.ibanVendeur = ibanVendeur;
		this.montant = montant;
	}
	public InfosBancaireDTO getInfosBancaireDTO() {
		return infosBancaireDTO;
	}
	public void setInfosBancaireDTO(InfosBancaireDTO infosBancaireDTO) {
		this.infosBancaireDTO = infosBancaireDTO;
	}
	public String getIbanVendeur() {
		return ibanVendeur;
	}
	public void setIbanVendeur(String ibanVendeur) {
		this.ibanVendeur = ibanVendeur;
	}
	public Double getMontant() {
		return montant;
	}
	public void setMontant(Double montant) {
		this.montant = montant;
	}
	
	
	
}
