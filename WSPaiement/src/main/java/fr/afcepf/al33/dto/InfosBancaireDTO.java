package fr.afcepf.al33.dto;

public class InfosBancaireDTO {

	private String numeroCarte;
	private Integer moisExpiration;
	private Integer anneeExpiration;
	private String cryptogramme;
	private Double montant;
	
	public InfosBancaireDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InfosBancaireDTO(String numeroCarte, int moisExpiration, int anneeExpiration,
			String cryptogramme, double montant) {
		super();
		this.numeroCarte = numeroCarte;
		this.moisExpiration = moisExpiration;
		this.anneeExpiration = anneeExpiration;
		this.cryptogramme = cryptogramme;
		this.montant = montant;
	}

	public String getNumeroCarte() {
		return numeroCarte;
	}

	public void setNumeroCarte(String numeroCarte) {
		this.numeroCarte = numeroCarte;
	}

	public Integer getMoisExpiration() {
		return moisExpiration;
	}

	public void setMoisExpiration(Integer moisExpiration) {
		this.moisExpiration = moisExpiration;
	}

	public Integer getAnneeExpiration() {
		return anneeExpiration;
	}

	public void setAnneeExpiration(Integer anneeExpiration) {
		this.anneeExpiration = anneeExpiration;
	}

	public String getCryptogramme() {
		return cryptogramme;
	}

	public void setCryptogramme(String cryptogramme) {
		this.cryptogramme = cryptogramme;
	}

	public Double getMontant() {
		return montant;
	}

	public void setMontant(Double montant) {
		this.montant = montant;
	}
}
