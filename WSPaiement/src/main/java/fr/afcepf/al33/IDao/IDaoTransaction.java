package fr.afcepf.al33.IDao;

import fr.afcepf.al33.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IDaoTransaction extends JpaRepository<Transaction, Integer> {

    Transaction findByHash(String hash);
}
