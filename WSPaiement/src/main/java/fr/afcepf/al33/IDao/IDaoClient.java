package fr.afcepf.al33.IDao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afcepf.al33.entity.Client;

public interface IDaoClient extends JpaRepository<Client, Integer> {

}
