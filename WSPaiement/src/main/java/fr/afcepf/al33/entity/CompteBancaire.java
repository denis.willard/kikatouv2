package fr.afcepf.al33.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="compte_bancaire")
public class CompteBancaire implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idCompte;
	
	@Column(name = "iban", nullable = false, length = 34)
	private String iban;

	@OneToMany(mappedBy = "compte")
	private List<InfosBancaire> cartes;
	
	@Column(name="solde", nullable = false)
	private double solde;
	
	public CompteBancaire() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CompteBancaire(Integer idCompte, String iban, List<InfosBancaire> cartes, double solde) {
		super();
		this.idCompte = idCompte;
		this.iban = iban;
		this.cartes = cartes;
		this.solde = solde;
	}


	public Integer getIdCompte() {
		return idCompte;
	}

	public void setIdCompte(Integer idCompte) {
		this.idCompte = idCompte;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public java.util.List<InfosBancaire> getCartes() {
		return cartes;
	}

	public void setCartes(java.util.List<InfosBancaire> cartes) {
		this.cartes = cartes;
	}

	public double getSolde() {
		return solde;
	}

	public void setSolde(double solde) {
		this.solde = solde;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
