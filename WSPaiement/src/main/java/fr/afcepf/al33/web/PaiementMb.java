package fr.afcepf.al33.web;


import java.io.IOException;
import java.io.Serializable;
import java.util.Optional;

import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import fr.afcepf.al33.IDao.IDaoTransaction;
import fr.afcepf.al33.entity.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.afcepf.al33.dto.AutorisationDTO;
import fr.afcepf.al33.dto.InfosBancaireDTO;
import fr.afcepf.al33.service.IService;


@Component("paiementMb")
@SessionScoped
public class PaiementMb implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Autowired
    IDaoTransaction daoTransaction;

    private InfosBancaireDTO infoPaiement = new InfosBancaireDTO();

    private String messageError = "";

    private String nomCarte = "";

    private Double montant = 0d;

    private String id;

    private Transaction transaction;

    @Autowired
    private IService servicePaiement;

    public void init() {
        messageError = "";
        transaction = daoTransaction.findByHash(id);
        montant = transaction.getMontant();
    }

	public void setDataDemo() {
        setNomCarte("Guillaume Nadrault");
		infoPaiement.setAnneeExpiration(2021);
		infoPaiement.setMoisExpiration(12);
		infoPaiement.setNumeroCarte("1111222233334444");
		infoPaiement.setCryptogramme("666");
	}

    public String payer() {
        infoPaiement.setMontant(transaction.getMontant());
        AutorisationDTO autorisation = servicePaiement.validationPaiement(infoPaiement);
        if (autorisation.isEstAccepte()) {
            messageError = "";
            servicePaiement.validerTransaction(transaction);
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            try {
                externalContext.redirect(transaction.getUrl() + "?banque=true");
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Redirection après paiement impossible!!!");
            }
        }
        messageError = autorisation.getMessage();
        return null;
    }

    public void annuler() throws IOException {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        externalContext.redirect(transaction.getUrl());
    }


    public InfosBancaireDTO getInfoPaiement() {
        return infoPaiement;
    }


    public void setInfoPaiement(InfosBancaireDTO infoPaiement) {
        this.infoPaiement = infoPaiement;
    }


    public IService getServicePaiement() {
        return servicePaiement;
    }


    public void setServicePaiement(IService servicePaiement) {
        this.servicePaiement = servicePaiement;
    }


    public static long getSerialversionuid() {
        return serialVersionUID;
    }


    public String getMessageError() {
        return messageError;
    }


    public void setMessageError(String messageError) {
        this.messageError = messageError;
    }


    public String getNomCarte() {
        return nomCarte;
    }


    public void setNomCarte(String nomCarte) {
        this.nomCarte = nomCarte;
    }

    public Double getMontant() {
        return montant;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }
}