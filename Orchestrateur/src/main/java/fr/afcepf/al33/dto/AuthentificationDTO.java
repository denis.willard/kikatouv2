package fr.afcepf.al33.dto;

import java.io.Serializable;

public class AuthentificationDTO implements Serializable {
	private static final long serialVersionUID = 1L;

    public AuthentificationDTO() {
    }

    public AuthentificationDTO(String login, String password, Integer idKikatou, String role) {
        this.login = login;
        this.password = password;
        this.idKikatou = idKikatou;
        this.role = role;
    }

    private String login;
    private String password;
    private Integer idKikatou;
    private String role;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getIdKikatou() {
        return idKikatou;
    }

    public void setIdKikatou(Integer idKikatou) {
        this.idKikatou = idKikatou;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
