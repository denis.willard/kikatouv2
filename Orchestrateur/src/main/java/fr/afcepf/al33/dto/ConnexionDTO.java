package fr.afcepf.al33.dto;

import dtoClients.ClientDTO;

import java.io.Serializable;

public class ConnexionDTO implements Serializable {
	private static final long serialVersionUID = 1L;

    public ConnexionDTO() {
    }

    private boolean connexionOK;
    private ClientDTO clientDTO;

    public ClientDTO getClientDTO() {
        return clientDTO;
    }

    public void setClientDTO(ClientDTO clientDTO) {
        this.clientDTO = clientDTO;
    }

    public boolean isConnexionOK() {
        return connexionOK;
    }

    public void setConnexionOK(boolean connexionOK) {
        this.connexionOK = connexionOK;
    }
}
