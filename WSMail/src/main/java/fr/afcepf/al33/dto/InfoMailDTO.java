package fr.afcepf.al33.dto;

import java.io.Serializable;
import java.util.Date;

public class InfoMailDTO implements Serializable {
	private static final long serialVersionUID = 1L;

    private String adressePrestataire;
    private String nomPrestataire;
    private String prenomPrestataire;
    private String dateDebutReservation;
    private String duree;
    private String nomClient;
    private String prenomClient;

    public InfoMailDTO() {
    }

    public String getAdressePrestataire() {
        return adressePrestataire;
    }

    public void setAdressePrestataire(String adressePrestataire) {
        this.adressePrestataire = adressePrestataire;
    }

    public String getNomPrestataire() {
        return nomPrestataire;
    }

    public void setNomPrestataire(String nomPrestataire) {
        this.nomPrestataire = nomPrestataire;
    }

    public String getPrenomPrestataire() {
        return prenomPrestataire;
    }

    public void setPrenomPrestataire(String prenomPrestataire) {
        this.prenomPrestataire = prenomPrestataire;
    }

    public String getDateDebutReservation() {
        return dateDebutReservation;
    }

    public void setDateDebutReservation(String dateDebutReservation) {
        this.dateDebutReservation = dateDebutReservation;
    }

    public String getDuree() {
        return duree;
    }

    public void setDuree(String duree) {
        this.duree = duree;
    }

    public String getNomClient() {
        return nomClient;
    }

    public void setNomClient(String nomClient) {
        this.nomClient = nomClient;
    }

    public String getPrenomClient() {
        return prenomClient;
    }

    public void setPrenomClient(String prenomClient) {
        this.prenomClient = prenomClient;
    }
}
