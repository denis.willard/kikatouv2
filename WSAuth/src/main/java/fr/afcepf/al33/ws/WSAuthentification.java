package fr.afcepf.al33.ws;

import fr.afcepf.al33.dto.AuthentificationDTO;
import fr.afcepf.al33.dto.AuthentificationRoleDTO;
import fr.afcepf.al33.entities.Authentification;
import fr.afcepf.al33.entities.Role;
import fr.afcepf.al33.service.api.IServiceAuth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/rest", headers = "Accept=application/json")
public class WSAuthentification {

    @Autowired
    private IServiceAuth serviceAuth;

    @PostMapping("/ajouter")
    public void ajouterAuth(@RequestBody AuthentificationDTO authentificationDTO) {
        Authentification authentification = new Authentification();
        authentification.setLogin(authentificationDTO.getLogin());
        authentification.setPassword(authentificationDTO.getPassword());
        authentification.setIdKikatou(authentificationDTO.getIdKikatou());
        Role role = serviceAuth.getRoleByLibelle(authentificationDTO.getRole());
        authentification.setRole(role);
        serviceAuth.ajouterAuth(authentification);
    }

    @GetMapping("/connexion/{login}/{password}")
    public AuthentificationRoleDTO connexionAuth(@PathVariable String login,@PathVariable String password) {
        Authentification authentification = serviceAuth.connexion(login, password);
        AuthentificationRoleDTO authentificationRoleDTO = new AuthentificationRoleDTO();
        if (authentification != null) {
            authentificationRoleDTO.setId(authentification.getIdKikatou());
            authentificationRoleDTO.setRole(authentification.getRole().getLibelle());
            authentificationRoleDTO.setAuth(true);
        } else {
            authentificationRoleDTO.setAuth(false);
        }
        return authentificationRoleDTO;
    }

    @DeleteMapping("/supprimer/{idKikatou}")
    public void supprimerAuth(@PathVariable Integer idKikatou) {
        serviceAuth.supprimerAuthentification(idKikatou);
    }
}
