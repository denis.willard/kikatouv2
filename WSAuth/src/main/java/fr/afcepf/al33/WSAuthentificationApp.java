package fr.afcepf.al33;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class WSAuthentificationApp extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(WSAuthentificationApp.class, args);
    }
}
