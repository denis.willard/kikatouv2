package fr.afcepf.al33.service.impl;

import fr.afcepf.al33.dao.api.IDaoAuthentification;
import fr.afcepf.al33.dao.api.IDaoRole;
import fr.afcepf.al33.entities.Authentification;
import fr.afcepf.al33.entities.Role;
import fr.afcepf.al33.service.api.IServiceAuth;
import fr.afcepf.al33.util.MD5Password;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.security.NoSuchAlgorithmException;

@Service
@Transactional
public class ServiceAuth implements IServiceAuth {


    @Autowired
    private IDaoAuthentification daoAuthentification;

    @Autowired
    private IDaoRole daoRole;

    @Override
    public Authentification ajouterAuth(Authentification authentification) {
        String premierHash = MD5Password.getEncodedPassword(authentification.getPassword());
        String secondHash = MD5Password.getEncodedPassword(premierHash);
        authentification.setPassword(secondHash);
        return daoAuthentification.save(authentification);
    }

    @Override
    public void supprimerAuthentification(Integer idKikatou) {
        Authentification authentification = daoAuthentification.findAuthentificationByIdKikatou(idKikatou);
        daoAuthentification.delete(authentification);
    }

    @Override
    public Authentification modifierAuthentification(Authentification authentification) {
        return daoAuthentification.save(authentification);
    }

    @Override
    public Authentification getAuthByIdKikatou(Integer id) {
        return daoAuthentification.findAuthentificationByIdKikatou(id);
    }

    @Override
    public Authentification connexion(String login, String password) {
        Authentification authentification = daoAuthentification.findAuthentificationByLogin(login);
        if (authentification != null) {
            try {
                if (MD5Password.testPassword(password, authentification.getPassword()))
                    return authentification;
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public Role getRoleByLibelle(String libelle) {
        return daoRole.findRoleByLibelle(libelle);
    }
}
