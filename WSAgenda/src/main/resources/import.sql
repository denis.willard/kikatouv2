INSERT INTO prestataire (id, nom, color) VALUES (1, 'DJ Morad', 'blue');
INSERT INTO prestataire (id, nom, color) VALUES (2, 'Cuisinier Guillaume', 'red');
INSERT INTO prestataire (id, nom, color) VALUES (3, 'Animateur Cyril', 'green');
INSERT INTO prestataire (id, nom, color) VALUES (4, 'Roque et son orchestre', 'yellow');
INSERT INTO prestataire (id, nom, color) VALUES (5, 'Sécurité Denis', 'cyan');
INSERT INTO prestataire (id, nom, color) VALUES (6, 'Photographe Laurent', 'orange');
INSERT INTO prestataire (id, nom, color) VALUES (7, 'Photographe Sylvain', 'pink');
INSERT INTO prestataire (id, nom, color) VALUES (8, 'Animateur Florian', 'magenta');
