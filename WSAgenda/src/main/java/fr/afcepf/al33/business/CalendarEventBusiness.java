package fr.afcepf.al33.business;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import fr.afcepf.al33.dao.CalendarEventDao;
import fr.afcepf.al33.dao.PrestataireDao;
import fr.afcepf.al33.dto.ListPrestataireReferences;
import fr.afcepf.al33.dto.LockInfos;
import fr.afcepf.al33.dto.Periode;
import fr.afcepf.al33.dto.PrestataireReference;
import fr.afcepf.al33.entity.CalendarEvent;
import fr.afcepf.al33.entity.Prestataire;

@Service
public class CalendarEventBusiness implements CalendarEventIBusiness {
	@Autowired
	private CalendarEventDao dao;

	@Autowired
	private PrestataireDao prestataireDao;

	@Override
	public List<CalendarEvent> tousCalendarEvents(Integer id) {

		List<CalendarEvent> result = null;
		if (id == null)
			result = (List<CalendarEvent>) dao.findAll();
		else {
			result = (List<CalendarEvent>) dao.findByPrestataireId(id);
		}
		return result;
		
	}

	@Override
	public List<CalendarEvent> lockedCalendarEvents(String lockedBy) {
		List<CalendarEvent> result = null;
		if (lockedBy == null)
			result = (List<CalendarEvent>) dao.findByLockedByNot("none");
		else {
			result = (List<CalendarEvent>) dao.findByLockedBy(lockedBy);
		}
		return result;
	}
	
	@Override
	public List<CalendarEvent> tousCalendarEventsByDate() {
		return (List<CalendarEvent>) dao.findAll();
	}

	@Override
	public CalendarEvent createOrUpdateCalendarEvent(CalendarEvent calendarEvent) {
		dao.save(calendarEvent);
		return calendarEvent; // contenant la clé auto-incrémentée, quand gérée !
	}

	@Override
	public ResponseEntity<?> deleteCalendarEvent(CalendarEvent calendarEvent) {
		// recherche du CalendarEvent par Id
		CalendarEvent e = dao.findById(calendarEvent.getId()).get();  // .get remonte exception si pas trouvé
		dao.delete(e);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@Override
	public ListPrestataireReferences getPrestatairesDisponibles(Periode periode) {
		// lire la liste de toutes les tranches horaires bloquées
		List<CalendarEvent> tousCalendarEvents = (List<CalendarEvent>) dao.findAll();

		// lire la liste complete des prestataires
		List<Prestataire> tousPrestataires = (List<Prestataire>) prestataireDao.findAll();

		ListPrestataireReferences result = new ListPrestataireReferences();
	
		// constitution de la liste des prestataires non disponibles pour la periode demandée
		
		Map<Integer, Integer> hm = new HashMap<>();	
		
		for (CalendarEvent c : tousCalendarEvents) {
			
			// si la date début de c est dans la periode, alors le prestaire n'est pas disponible
			if (! c.isInsidePeriode(periode.getDateDebut(), periode.getDateFin())) { continue; } 
			hm.put(c.getPrestataireId(), c.getPrestataireId());
		}

		// construire la liste 
		for (Prestataire p : tousPrestataires) {
			if (hm.get(p.getId()) == null )
				result.getPrestatairesReferences().add(new PrestataireReference(p.getId()));
		}
		
		return result;
	}
	
	@Override
	public boolean estLibre(LockInfos lockInfos) {
		
		boolean result = true;
		
		// lire la liste de toutes les tranches horaires bloquées
		List<CalendarEvent> tousCalendarEvents = (List<CalendarEvent>) dao.findAll();

		// recherche d'une réservation sur la période
		
		for (CalendarEvent c : tousCalendarEvents) {
			
			if (! c.isInsidePeriode(lockInfos.getStart(), lockInfos.getEnd())) { continue; } 
			if ( c.getPrestataireId() != lockInfos.getIdPrestataire() ) { continue; }
			
			// si la date début de c est dans la periode, alors le prestaire n'est pas disponible
			result = false;
		}
		
		return result;		
	}


	@Override
	public List<CalendarEvent> getCalendarEventLockedByMe(LockInfos lockInfos) {
		List<CalendarEvent> tousCalendarEvents = (List<CalendarEvent>) dao.findAll();
		
		/* Je veux conserver tous les CalendarEvents qui touchent à la période donnée en argument */ 
		List<CalendarEvent> result = tousCalendarEvents.stream()
				  .filter((d)-> d.isInsidePeriode(lockInfos.getStart(), lockInfos.getEnd()))
				  .filter((d)-> d.getLockedBy().equals(lockInfos.getLockedBy()))
				  .filter((d)-> d.getPrestataireId() == lockInfos.getIdPrestataire())
				  .collect(Collectors.toList());	
		return result;
	}
	
	@Override
	public boolean lockCalendarEvent(LockInfos lockInfos) {

		boolean result = true;
		CalendarEvent savedCalendarEvent = null;
		
		if (estLibre(lockInfos)) {
			// insérer un CalendarEvent
			CalendarEvent ce = new CalendarEvent();
			Prestataire prestataire = prestataireDao.findById(lockInfos.getIdPrestataire()).get();
			
			if (prestataire != null) {
					
				ce.setEventId(-1);
				ce.setPrestataireId(lockInfos.getIdPrestataire());
				ce.setTitle(prestataire.getNom());
				ce.setStart(lockInfos.getStart());
				ce.setEnd(lockInfos.getEnd());
				ce.setLockedBy(lockInfos.getLockedBy());
				ce.setColor(prestataire.getColor());
				
				savedCalendarEvent = dao.save(ce);
				result = (savedCalendarEvent != null);
			}
			else {
				// prestataire non répertorié
				result = false;
			}
			
		}
		
		return result;
	}

	
	@Override
	public boolean unlockCalendarEvent(LockInfos lockInfos) {

		boolean result = true;
		// lecture de la liste des CalendarEvents satisfaisant la période demandée, pour le prestataire demandé
		List<CalendarEvent> calendarEvents = getCalendarEventLockedByMe(lockInfos);

		// réservation des CalendarEvent 
		for (CalendarEvent calendarEvent : calendarEvents) {
			dao.delete(calendarEvent);
		}

		return result; 
	}
	
}
