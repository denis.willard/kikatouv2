package fr.afcepf.al33.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.afcepf.al33.entity.CalendarEvent;

public interface CalendarEventDao  extends CrudRepository<CalendarEvent, Integer> {

	List<CalendarEvent> findByLockedBy(String lockedBy);
	List<CalendarEvent> findByLockedByNot(String lockedBy);
	List<CalendarEvent> findByPrestataireId(Integer prestataireId);
	
}
