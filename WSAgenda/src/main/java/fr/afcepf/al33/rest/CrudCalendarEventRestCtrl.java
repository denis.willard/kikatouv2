package fr.afcepf.al33.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.afcepf.al33.business.CalendarEventBusiness;
import fr.afcepf.al33.dto.LockInfos;
import fr.afcepf.al33.entity.CalendarEvent;

@RestController
@RequestMapping(value="/dates", headers="Accept=application/json")
public class CrudCalendarEventRestCtrl {
	
	@Autowired
	private CalendarEventBusiness proxy;

	@PostMapping(value="/post")
	public CalendarEvent createOrUpdateCalendarEvent(@RequestBody CalendarEvent calendarEvent) {
		return proxy.createOrUpdateCalendarEvent(calendarEvent);
	}
	
	@DeleteMapping(value="/delete")	
	public ResponseEntity<?> deleteCalendarEvent(@RequestBody CalendarEvent calendarEvent) {
		return proxy.deleteCalendarEvent(calendarEvent);
	}

	@PostMapping(value = "/lock")
	public boolean lockCalendarEvent(@RequestBody LockInfos lockInfos)
	{
		return proxy.lockCalendarEvent(lockInfos);
	}

	@PostMapping(value = "/unlock")
	public boolean unlockCalendarEvent(@RequestBody LockInfos lockInfos)
	{
		return proxy.unlockCalendarEvent(lockInfos);
	}
	
}
